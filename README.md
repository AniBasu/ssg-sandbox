# README

Sandbox for GitLab pages with various static site generators (SSG). After the stuff is tested here, the HowTo instructions go to live at [Writing Technically](https://writing-technically.readthedocs.io/en/latest/).

Each tested SSG has a branch with all the required files to get GitLab to build a Pages site from the source files.

-  [ ] Sphinx
-  [ ] MkDocs
-  [ ] Jekyll
-  [ ] ...